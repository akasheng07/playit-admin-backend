package com.akash.playIt.model.repo;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.akash.playIt.model.Player;
import com.akash.playIt.model.Team;

public interface PlayerRepo extends JpaRepository<Player, Long>{

	@Query(
			  value = "SELECT * FROM player  WHERE team_name =  ?1", 
			  nativeQuery = true)
			public List<Player> findAllTeamsByTeamname(String teamname);
	
	
	 @Modifying
	    @Transactional
	@Query(value="DELETE from player WHERE team_name=?1",nativeQuery=true)
	public void delAllPlayersByTeamname(String teamname);
}
