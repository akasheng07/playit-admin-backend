package com.akash.playIt.model.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akash.playIt.model.MapContestFixture;

public interface MapContestFixtureRepo extends JpaRepository<MapContestFixture, Long>{

	List<MapContestFixture> findByMatchid(Long id);
}
