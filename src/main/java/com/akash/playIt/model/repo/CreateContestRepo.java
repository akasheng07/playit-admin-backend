package com.akash.playIt.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akash.playIt.model.CreateContest;

public interface CreateContestRepo extends JpaRepository<CreateContest, Long>{

	}
