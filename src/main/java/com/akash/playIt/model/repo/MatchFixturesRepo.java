package com.akash.playIt.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akash.playIt.model.MatchFixtures;

public interface MatchFixturesRepo extends JpaRepository<MatchFixtures, Long> {

}
