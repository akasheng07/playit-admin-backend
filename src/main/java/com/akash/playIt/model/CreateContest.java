package com.akash.playIt.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "createcontest")
public class CreateContest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String contestname;
	private int maxsize;
	private int minsize;
	private int prizemoney;
	private int enteryfee;
	private int rank1prize;
	private int rank2prize;
	private int rank3prize;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getContestname() {
		return contestname;
	}
	public void setContestname(String contestname) {
		this.contestname = contestname;
	}
	public int getMaxsize() {
		return maxsize;
	}
	public void setMaxsize(int maxsize) {
		this.maxsize = maxsize;
	}
	public int getMinsize() {
		return minsize;
	}
	public void setMinsize(int minsize) {
		this.minsize = minsize;
	}
	public int getPrizemoney() {
		return prizemoney;
	}
	public void setPrizemoney(int prizemoney) {
		this.prizemoney = prizemoney;
	}
	public int getEnteryfee() {
		return enteryfee;
	}
	public void setEnteryfee(int enteryfee) {
		this.enteryfee = enteryfee;
	}
	public int getRank1prize() {
		return rank1prize;
	}
	public void setRank1prize(int rank1prize) {
		this.rank1prize = rank1prize;
	}
	public int getRank2prize() {
		return rank2prize;
	}
	public void setRank2prize(int rank2prize) {
		this.rank2prize = rank2prize;
	}
	public int getRank3prize() {
		return rank3prize;
	}
	public void setRank3prize(int rank3prize) {
		this.rank3prize = rank3prize;
	}
	@Override
	public String toString() {
		return "CreateContest [id=" + id + ", contestname=" + contestname + ", maxsize=" + maxsize + ", minsize="
				+ minsize + ", prizemoney=" + prizemoney + ", enteryfee=" + enteryfee + ", rank1prize=" + rank1prize
				+ ", rank2prize=" + rank2prize + ", rank3prize=" + rank3prize + "]";
	}
	
	
}
