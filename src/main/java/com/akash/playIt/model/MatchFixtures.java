package com.akash.playIt.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MatchFixtures {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	public Long team1id;
	public String team1name;
	public Long team2id;
	public String team2name;
	public String matchdatetime;
	public String venue;
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String stage;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTeam1id() {
		return team1id;
	}
	public void setTeam1id(Long team1id) {
		this.team1id = team1id;
	}
	public String getTeam1name() {
		return team1name;
	}
	public void setTeam1name(String team1name) {
		this.team1name = team1name;
	}
	public Long getTeam2id() {
		return team2id;
	}
	public void setTeam2id(Long team2id) {
		this.team2id = team2id;
	}
	public String getTeam2name() {
		return team2name;
	}
	public void setTeam2name(String team2name) {
		this.team2name = team2name;
	}
	
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getMatchdatetime() {
		return matchdatetime;
	}
	public void setMatchdatetime(String matchdatetime) {
		this.matchdatetime = matchdatetime;
	}
	@Override
	public String toString() {
		return "MatchFixtures [id=" + id + ", team1id=" + team1id + ", team1name=" + team1name + ", team2id=" + team2id
				+ ", team2name=" + team2name + ", matchdatetime=" + matchdatetime + ", venue=" + venue + ", stage="
				+ stage + "]";
	}
	

	
}
