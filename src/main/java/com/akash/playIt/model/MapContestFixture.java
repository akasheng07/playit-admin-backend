package com.akash.playIt.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mapcontestfixture")
public class MapContestFixture {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long matchid;
	private String matchname;
	private Long contestid;
	private String contestname;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMatchid() {
		return matchid;
	}
	public void setMatchid(Long matchid) {
		this.matchid = matchid;
	}
	public String getMatchname() {
		return matchname;
	}
	public void setMatchname(String matchname) {
		this.matchname = matchname;
	}
	public Long getContestid() {
		return contestid;
	}
	public void setContestid(Long contestid) {
		this.contestid = contestid;
	}
	public String getContestname() {
		return contestname;
	}
	public void setContestname(String contestname) {
		this.contestname = contestname;
	}
	@Override
	public String toString() {
		return "MapContestFixture [id=" + id + ", matchid=" + matchid + ", matchname=" + matchname + ", contestid="
				+ contestid + ", contestname=" + contestname + "]";
	}
}
