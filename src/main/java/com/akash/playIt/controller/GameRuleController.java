package com.akash.playIt.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akash.playIt.GameSystem.GameRules;

@RestController
@RequestMapping("/playIt/api/v1/admin/gameRule")
@CrossOrigin("*")
public class GameRuleController {

	@Autowired
	private GameRules gameRules;
	
	@GetMapping("/app-info")
	public ResponseEntity<Map<String, String>> getAppInfo(){
		Map<String,String> appInfo = new HashMap<>();
		appInfo.put("batting run points", gameRules.getBattingPointsRun());
		appInfo.put("batting boundary points", gameRules.getBattingBoundaryPoints());
		appInfo.put("bowling wicket points", gameRules.getBowlingPointsWicket());
		appInfo.put("bowling boundary points", gameRules.getBowlingPointsBoundary());
		appInfo.put("fielding boundary points", gameRules.getFieldingpointsBoundary());
		appInfo.put("other points caption", gameRules.getOtherPointsCaption());
		return ResponseEntity.ok(appInfo);
	}
}
