package com.akash.playIt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akash.playIt.model.CreateContest;
import com.akash.playIt.service.CreateContestService;

@RestController
@RequestMapping("/playIt/api/vi/createContest")
@CrossOrigin("*")
public class CreateContestController {

	@Autowired 
	private CreateContestService createContestService;
	
	//create
	@PostMapping("/")
	public Object create(@RequestBody CreateContest createContest) {
		return this.createContestService.create(createContest);
	}
	
	
	//get all
	@GetMapping("/")
	public List<CreateContest> getAll(){
		return this.createContestService.getAll();
	}
	
	//get by id
	@GetMapping("/{id}")
	public Object getById(@PathVariable Long id) {
		return this.createContestService.getById(id);
	}
	
	//delete
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		this.createContestService.delete(id);
	}
	
	//update
	@PutMapping("/")
	public Object update(@RequestBody CreateContest createContest) {
		return this.createContestService.update(createContest);
	}
	
}
