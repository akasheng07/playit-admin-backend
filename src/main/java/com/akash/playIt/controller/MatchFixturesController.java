package com.akash.playIt.controller;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akash.playIt.model.MatchFixtures;
import com.akash.playIt.service.MatchFixturesServices;


@RestController
@RequestMapping("/playIt/api/v1/matchFixtures")
@CrossOrigin("*")
public class MatchFixturesController {

	@Autowired
	public MatchFixturesServices matchFixturesServices;
	
	//get all
	@GetMapping("/")
	public List<MatchFixtures> getAll(){
	 return	matchFixturesServices.getAllMatchFixtures();
	}
	
	
	//create
	@PostMapping("/")
	public MatchFixtures create(@RequestBody MatchFixtures matchFixtures) {
		
		return matchFixturesServices.create(matchFixtures);
	}
	
	//delete
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		matchFixturesServices.deleteMatchFixture(id);
	}
	
	//update
	@PutMapping("/")
	public Object update(@RequestBody MatchFixtures matchFixtures) {
		return matchFixturesServices.update(matchFixtures);
	}
	
	//get by id
	@GetMapping("/{id}")
	public Optional<MatchFixtures> getById(@PathVariable Long id) {
		return matchFixturesServices.getById(id);
	}
}
