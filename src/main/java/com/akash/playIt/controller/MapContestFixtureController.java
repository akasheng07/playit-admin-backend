package com.akash.playIt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akash.playIt.model.MapContestFixture;
import com.akash.playIt.service.MapContestFixtureService;

@RestController
@RequestMapping("/playIt/api/vi/mapcontestfixture")
@CrossOrigin("*")
public class MapContestFixtureController {

	
	@Autowired
	private MapContestFixtureService mapContestFixtureService;
	
	//create
	@PostMapping("/")
	public Object create(@RequestBody MapContestFixture mapContestFixture) {
		return mapContestFixtureService.create(mapContestFixture);
	}
	
	//get all
	@GetMapping("/")
	public List<MapContestFixture> getAll(){
		return mapContestFixtureService.getAll();
	}
	
	//get by matchid
	@GetMapping("/{id}")
	public List<MapContestFixture> getbymatchid(@PathVariable Long id){
		return mapContestFixtureService.getAllByMatchId(id);
	}
	
	//del by id
	@DeleteMapping("/{id}")
	public void deletebyid(@PathVariable Long id) {
		mapContestFixtureService.deleteById(id);
	}
}
