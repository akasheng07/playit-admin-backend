package com.akash.playIt.service;

import java.util.List;

import com.akash.playIt.model.MapContestFixture;

public interface MapContestFixtureService {

	
	public Object create(MapContestFixture mapContestFixture);
	
	public List<MapContestFixture> getAll();
	
	public void deleteById(Long id);
	
	public List<MapContestFixture> getAllByMatchId(Long matchid);
	
}
