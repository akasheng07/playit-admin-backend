package com.akash.playIt.service;

import java.util.List;
import java.util.Optional;

import com.akash.playIt.model.MatchFixtures;

public interface MatchFixturesServices {

	//create
	public MatchFixtures create(MatchFixtures matchFixtures);
	
	//get all
	public List<MatchFixtures> getAllMatchFixtures();

	//delete 
	public void deleteMatchFixture(Long id);
	
	//update
	public Object update(MatchFixtures matchFixtures);
	
	//get by id
	public Optional<MatchFixtures> getById(Long id);
}
