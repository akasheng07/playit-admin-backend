package com.akash.playIt.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akash.playIt.model.CreateContest;
import com.akash.playIt.model.MapContestFixture;
import com.akash.playIt.model.MatchFixtures;
import com.akash.playIt.model.repo.CreateContestRepo;
import com.akash.playIt.model.repo.MapContestFixtureRepo;
import com.akash.playIt.model.repo.MatchFixturesRepo;
import com.akash.playIt.service.CreateContestService;
import com.akash.playIt.service.MapContestFixtureService;
import com.akash.playIt.service.MatchFixturesServices;

@Service
public class MapContestFixtureServiceImpl implements MapContestFixtureService{

	@Autowired
	private MapContestFixtureRepo mapContestFixtureRepo;
	
	@Autowired
	private CreateContestService createContestService;
	
	@Autowired
	private MatchFixturesServices matchFixtrueService;
	
	
	
	@Override
	public Object create(MapContestFixture mapContestFixture) {
		
		
		 
		//setting the contest name
		Optional<CreateContest> contest =   createContestService.getById(mapContestFixture.getContestid());
		CreateContest tempc=new CreateContest();
		if(contest.isPresent()) {
			tempc = contest.get();
			mapContestFixture.setContestname(tempc.getContestname());
		}
		System.out.println(tempc.getContestname());
		
		
		//setting the match name
		Optional<MatchFixtures> match = matchFixtrueService.getById(mapContestFixture.getMatchid());
		MatchFixtures tempmatch = new MatchFixtures();
		if(match.isPresent()) {
			tempmatch=match.get();
			mapContestFixture.setMatchname(tempmatch.getTeam1name()+" vs "+tempmatch.getTeam2name());
		}
		
		
		
		return mapContestFixtureRepo.save(mapContestFixture);
	}

	@Override
	public List<MapContestFixture> getAll() {
		return mapContestFixtureRepo.findAll();
	}

	@Override
	public void deleteById(Long id) {
		mapContestFixtureRepo.deleteById(id);
		
	}

	@Override
	public List<MapContestFixture> getAllByMatchId(Long matchid) {
		
		return mapContestFixtureRepo.findByMatchid(matchid);
	}

}
