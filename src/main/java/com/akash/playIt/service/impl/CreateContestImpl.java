package com.akash.playIt.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akash.playIt.model.CreateContest;
import com.akash.playIt.model.repo.CreateContestRepo;
import com.akash.playIt.service.CreateContestService;

@Service
public class CreateContestImpl implements CreateContestService{

	@Autowired
	private CreateContestRepo createContestRepo;
	
	@Override
	public Object create(CreateContest createContest) {
		return createContestRepo.save(createContest);
	}

	@Override
	public List<CreateContest> getAll() {
		return createContestRepo.findAll();
	}

	@Override
	public void delete(Long id) {
		createContestRepo.deleteById(id);
	}

	@Override
	public Object update(CreateContest createContest) {
		return createContestRepo.save(createContest);
	}

	@Override
	public Optional<CreateContest> getById(Long id) {
		return createContestRepo.findById(id);
		
	}

}
