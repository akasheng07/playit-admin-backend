package com.akash.playIt.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akash.playIt.model.MatchFixtures;
import com.akash.playIt.model.repo.MatchFixturesRepo;
import com.akash.playIt.service.MatchFixturesServices;

@Service
public class MatchFixturesServicesImpl implements MatchFixturesServices{

	@Autowired
	public MatchFixturesRepo matchFixturesRepo;
	
	@Override
	public MatchFixtures create(MatchFixtures matchFixtures) {
		return matchFixturesRepo.save(matchFixtures);
	}

	@Override
	public List<MatchFixtures> getAllMatchFixtures() {
		return matchFixturesRepo.findAll();
	}

	@Override
	public void deleteMatchFixture(Long id) {
		matchFixturesRepo.deleteById(id);
		
	}

	@Override
	public Object update(MatchFixtures matchFixtures) {
		
		return matchFixturesRepo.save(matchFixtures);
	}

	@Override
	public Optional<MatchFixtures> getById(Long id) {
		return matchFixturesRepo.findById(id);
	}

}
