package com.akash.playIt.service;

import java.util.List;
import java.util.Optional;

import com.akash.playIt.model.CreateContest;



public interface CreateContestService {

	//create
	public Object  create(CreateContest createContest);
	
	//get all
	public List<CreateContest> getAll();
	
	//delete
	public void delete(Long id);
	
	//update
	public Object update(CreateContest createContest);
	
	//get by id
	public Optional<CreateContest> getById(Long id);
}
